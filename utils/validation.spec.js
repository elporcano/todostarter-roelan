import { validateTodoEntry } from "./validation";
import * as error from "./error.msg";

describe("validateTodoEntry test", () => {
  test("it should accept if (id) and (title) is string type", () => {
    expect(validateTodoEntry("1", { title: "worked" })).toEqual({
      status: "success",
      message: []
    });
  });

  test("it should not accept if (id) and (title) is not string", () => {
    expect(validateTodoEntry("11", 222)).toEqual({
      status: "failed",
      message: [error.msg_val]
    });
  });
  test("it should not accept if (id) is undefined", () => {
    expect(validateTodoEntry(undefined, { title: "clear" })).toEqual({
      status: "failed",
      message: [error.msg_id]
    });
  });
  test("it should (title) has minimum length of 5", () => {
    expect(validateTodoEntry("1", { title: "cl123213" })).toEqual({
      status: "success",
      message: []
    });
  });
  test("it should not accept if (title) is less than 5 length", () => {
    expect(validateTodoEntry("1", { title: "cl11" })).toEqual({
      status: "failed",
      message: [error.msg_title]
    });
  });
});
