import * as error from "./error.msg";
/**
 * validate Todo object
 * Todo Object looks like:
 * {
 *  id: string,
 *  title: string
 * }
 * - title min length should be 5
 * - should have id
 * - id and title should be string
 *
 * @param {Object} todo
 */
export function validateTodoEntry(id, todo) {
  let _error = {
    status: "failed" || "success",
    message: []
  };

  if (id === undefined || typeof id !== "string") {
    _error.message.push(error.msg_id);
  } else if (
    todo === undefined ||
    Object.values(todo).includes("") ||
    Object.values(todo).includes(null) ||
    Object.keys(todo).length === 0
  ) {
    _error.message.push(error.msg_val);
  } else if (
    !todo.hasOwnProperty("title") ||
    todo.title.length < 5 ||
    typeof todo.title !== "string"
  ) {
    _error.message.push(error.msg_title);
  }

  Object.keys(_error.message).length > 0
    ? (_error.status = "failed")
    : (_error.status = "success");
  return _error;
}
