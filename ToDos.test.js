import ToDos from "./ToDos";
import * as error from "./utils/error.msg";

describe("ToDo tests", () => {
  let todos;
  let current_date;

  beforeAll(() => {
    const todoList = {};

    todos = new ToDos(todoList);
    current_date = new Date().toISOString();
  });

  it("should create", () => {
    todos.create({
      title: "bug create func",
      todo_at: "2020-03-18",
      created_at: current_date
    });
    todos.create({
      title: "fix create func",
      todo_at: "2020-03-18",
      created_at: current_date
    });
    todos.create({
      title: "define update func",
      todo_at: "2020-03-18",
      created_at: current_date
    });

    expect(todos.todoList).toEqual({
      "1": {
        title: "bug create func",
        todo_at: "2020-03-18",
        created_at: current_date
      },
      "2": {
        title: "fix create func",
        todo_at: "2020-03-18",
        created_at: current_date
      },
      "3": {
        title: "define update func",
        todo_at: "2020-03-18",
        created_at: current_date
      }
    });
  });

  it("should update", () => {
    const id = "2";
    const new_val = {
      title: "merge create func",
      todo_at: "2020-03-20",
      created_at: current_date
    };
    todos.update(id, new_val);

    expect(todos.todoList).toEqual({
      "1": {
        title: "bug create func",
        todo_at: "2020-03-18",
        created_at: current_date
      },
      "2": {
        title: "merge create func",
        todo_at: "2020-03-20",
        created_at: current_date
      },
      "3": {
        title: "define update func",
        todo_at: "2020-03-18",
        created_at: current_date
      }
    });
  });

  it("should throw error if (update) has no paramater were passed", () => {
    expect(todos.update()).toEqual({
      status: "failed",
      message: [error.msg_id]
    });
  });

  it("should delete", () => {
    todos.delete("1");
    todos.delete("2");

    expect(todos.todoList).toEqual({
      "3": {
        title: "define update func",
        todo_at: "2020-03-18",
        created_at: current_date
      }
    });
  });

  it("should throw error if (delete) has no paramater were passed", () => {
    expect(() => todos.delete()).toThrowError("ID is Required");
  });

  it("should get all", () => {
    const getAll = todos.getAll();
    expect(getAll).toEqual(todos.todoList);
  });

  it("should get one", () => {
    const getOne = todos.getOne("3");
    expect(getOne).toEqual({
      title: "define update func",
      todo_at: "2020-03-18",
      created_at: current_date
    });
  });
  it("should throw error if (get one) has no paramater were passed", () => {
    const getOne = todos.getOne;
    expect(getOne).toThrowError("ID is Required");
  });
  it("should throw error if (id) paramater is not exist", () => {
    expect(() => todos.getOne("1")).toThrowError("ID not exist");
  });
});
