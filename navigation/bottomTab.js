import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Ionicons from "react-native-vector-icons/Ionicons";

import { CreateScreen } from "./../screens/index";
import ListStackScreen from "./listStack";

const Tab = createBottomTabNavigator();

export default function BottomTabNavigation() {
  return (
    <Tab.Navigator
      initialRouteName="Todo"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          if (route.name === "Todo") {
            iconName = focused ? "md-list" : "md-list";
          } else {
            iconName = focused ? "md-add" : "md-add";
          }
          return <Ionicons name={iconName} size={size} color={color} />;
        }
      })}
      tabBarOptions={{
        activeTintColor: "#e6b800",
        inactiveTintColor: "gray",
        labelStyle: { color: "#332900", fontSize: 14 }
      }}
    >
      <Tab.Screen
        name="Todo"
        component={ListStackScreen}
        options={{ title: "ToDo list" }}
      />
      <Tab.Screen
        name="Create"
        component={CreateScreen}
        options={{ title: "Create" }}
      />
    </Tab.Navigator>
  );
}
