import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { ListScreen, DetailsScreen, UpdateScreen } from './../screens/index';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function ListStackScreen() {
    return (
        <Stack.Navigator >
            <Tab.Screen name="Todo" component={ListScreen} options={{ headerShown: false }} />
            <Tab.Screen name="Details" component={DetailsScreen} options={{ title: 'Details' }} />
            <Tab.Screen name="Update" component={UpdateScreen} options={{ headerShown: false, title: 'Update' }} />
        </Stack.Navigator>
    )
}