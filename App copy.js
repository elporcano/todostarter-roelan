import React, { Component } from 'react';
import { FlatList, Text, StyleSheet } from 'react-native';

const rows = {
        0: { text: 'View' },
        1: { text: 'Text' },
        2: { text: 'Image' },
        3: { text: 'ScrollView' },
        4: { text: 'ListView' }
};

const fn_rows = () =>{
    return Object.keys(rows).map(elem => {
      return {
         key: elem,
         ...rows[elem]
      }
    });
};



const extractKey = ({ key }) => key;

export default class App extends Component {

  renderItem = ({item}) => {
    return (
      <Text style={styles.row}>
        {item.text} {item.key}
      </Text>
    )
  }

  render() {
    return (
      <FlatList
        style={styles.container}
        data={fn_rows()}
        renderItem={this.renderItem}
        keyExtractor={extractKey}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    padding: 15,
    marginBottom: 5,
    backgroundColor: 'skyblue',
  },
})