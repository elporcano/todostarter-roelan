### Week 3

#### Objectives

- Improve on concepts already learned
- Learn React state management library - Redux

#### Instructions

- Sprint plan with mentor at start of week
- Setup and implement redux store
- Setup state management properly
- Setup redux-persist
- If there's more time try out different ways to persist redux state
