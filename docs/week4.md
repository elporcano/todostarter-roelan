### Week 4

#### Objectives

- Improve on concepts already learned
- Develop an eye for good design
- Improve previously built app

#### Instructions

- Sprint plan with mentor at start of week
- Improve UI of todo app

#### Bonus

- Use firebase firestore to handle todos
- Try, Apply and Learn redux-saga
- Try, Apply and Learn redux-observable
