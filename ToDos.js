import { validateTodoEntry } from "./utils/validation";

export default class Todos {
  constructor(todoList) {
    this.todoList = todoList;
  }

  /** /
   *
   * @param {object} params = {
   *   '1': {
   *     title: 'sample', todo_at: '2020-18-03', created_at: new Date()
   *   },
   *   '2': {
   *     title: 'sample2', todo_at: '2020-18-04', created_at: new Date()
   *   }
   * }
   */
  create(params) {
    const res_valid = validateTodoEntry("", params);
    if (res_valid.status === "success") {
      const gen_id = Object.keys(this.todoList);
      const get_max = gen_id.length === 0 ? 1 : Math.max(...gen_id) + 1;
      this.todoList[get_max] = { ...params };
    }
    return res_valid;
  }

  update(id, value) {
    const res_valid = validateTodoEntry(id, value);
    if (res_valid.status === "success") {
      this.todoList[id] = value;
    }
    return res_valid;
  }
  delete(id) {
    if (id) {
      delete this.todoList[id];
      return;
    }
    throw "ID is Required";
  }

  getAll() {
    return this.todoList;
  }

  getOne(id) {
    if (id) {
      if (this.todoList.hasOwnProperty(id)) {
        return this.todoList[id];
      }
      throw "ID not exist";
    }
    throw "ID is Required";
  }
}
