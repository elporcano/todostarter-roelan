import { StyleSheet } from "react-native";

export default StyleSheet.create({
  buttonContainer: {
    width: "15%",
    textAlign: "center"
  },
  labelStyles: {
    color: "#f2f2f2",
    textAlign: "center"
  }
});
