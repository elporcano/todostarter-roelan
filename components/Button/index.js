import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import istyles from "./style";

class Button extends Component {
  render() {
    const { styles, onPress, children, label, labelStyles } = this.props;
    const _buttonStyles = [istyles.buttonContainer, styles];
    const _labelStyles = [istyles.labelStyles, labelStyles];
    return (
      <TouchableOpacity style={_buttonStyles} onPress={onPress}>
        <View>{children || <Text style={_labelStyles}>{label}</Text>}</View>
      </TouchableOpacity>
    );
  }
}

Button.propTypes = {
  styles: PropTypes.object,
  labelStyles: PropTypes.object,
  onPress: PropTypes.func.isRequired,
  children: PropTypes.any,
  label: PropTypes.string
};

Button.defaultProps = {
  styles: {},
  labelStyles: {},
  label: ""
};

export default Button;
