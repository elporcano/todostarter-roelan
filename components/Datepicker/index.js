import React, { Component } from "react";
import { Text, View, Button, Platform, TouchableOpacity } from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import styles from "./style";

class Datepicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: this.props.setDate || null,
      show: false
    };
  }

  onChange = (event, selectDate) => {
    const selectedDate = selectDate;
    this.setState({
      show: Platform.OS === "ios",
      date: selectedDate
    });
    //set value to Form component
    this.props.onChangeDate(selectedDate);
  };

  showDatepicker = () => {
    this.setState({ show: !this.state.show });
  };

  renderDateText() {
    const { date } = this.state;
    const textDate = date ? new Date(date).toDateString() : "Select date";
    const colorDate = date ? "black" : "#a7a7ab";
    return (
      <Text style={[styles.dateText, { color: colorDate }]}>{textDate}</Text>
    );
  }

  render() {
    const { date, show } = this.state;
    console.log("date", date);

    return (
      <React.Fragment>
        <TouchableOpacity
          style={styles.field}
          onPress={() => this.showDatepicker()}
        >
          <View style={[styles.input, { flexDirection: "row" }]}>
            {this.renderDateText()}
            <Text style={{ textAlign: "right", width: "50%" }}> >> </Text>
          </View>
        </TouchableOpacity>
        {show && (
          <DateTimePicker
            timeZoneOffsetInMinutes={0}
            value={date !== null ? new Date(date) : new Date()}
            mode="date"
            display="default"
            onChange={(e, s) => this.onChange(e, s)}
          />
        )}
      </React.Fragment>
    );
  }
}

export default Datepicker;
