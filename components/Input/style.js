import { StyleSheet } from "react-native";

export default StyleSheet.create({
  field: {
    borderColor: "#4d3d00",
    borderWidth: 1,
    marginTop: 10,
    borderRadius: 6
  },
  input: {
    padding: 12,
    fontSize: 18
  }
});
