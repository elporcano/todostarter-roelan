import React, { Component } from "react";
import { View, Text, TouchableOpacity, TextInput } from "react-native";
import PropTypes from "prop-types";
import istyles from "./style";

class Input extends Component {
  render() {
    const {
      styles,
      placeholder,
      onChangeText,
      value,
      inputStyles
    } = this.props;
    const _containerStyles = [istyles.field, styles];
    const _inputStyles = [istyles.input, inputStyles];

    return (
      <View style={_containerStyles}>
        <TextInput
          placeholder={placeholder}
          style={_inputStyles}
          onChangeText={onChangeText}
          value={value}
        ></TextInput>
      </View>
    );
  }
}

Input.propTypes = {
  styles: PropTypes.object,
  inputStyles: PropTypes.object,
  onChangeText: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

Input.defaultProps = {
  styles: {},
  inputStyles: {},
  placeholder: ""
};

export default Input;
