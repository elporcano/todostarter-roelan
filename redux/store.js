import { createStore } from "redux";
import combineReducer from "./combineReducer";

const preLoadState = {};
const store = createStore(combineReducer, preLoadState);

export default store;
