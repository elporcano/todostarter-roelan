import {
  UPDATE_TODO,
  CREATE_TODO,
  DELETE_TODO,
  RESET_FORM
} from "./actionType";
import ToDos from "../../ToDos";

const current_date = new Date().toString();
const todos = new ToDos({
  "1": {
    title: "bug create func",
    todo_at: "2020-03-18",
    created_at: current_date
  }
});
const initial_state = {
  data: todos.todoList,
  currentId: null,
  isComplete: false,
  errors: []
};

export default function todoReducer(state = initial_state, action) {
  let err;
  switch (action.type) {
    case CREATE_TODO:
      err = todos.create(action.payload);
      if (err.status === "failed") {
        return {
          ...state,
          errors: [...err.message]
        };
      }
      return {
        ...state,
        isComplete: true,
        data: { ...todos.todoList }
      };
      break;
    case DELETE_TODO:
      todos.delete(action.payload);
      return {
        ...state,
        data: { ...todos.todoList }
      };
      break;
    case UPDATE_TODO:
      err = todos.update(action.payload.id, action.payload.data);
      if (err.status === "failed") {
        return {
          ...state,
          errors: [...err.message]
        };
      }
      return {
        ...state,
        isComplete: true,
        data: { ...todos.todoList }
      };
      break;
    case RESET_FORM:
      return {
        ...state,
        isComplete: false,
        errors: []
      };
    default:
      return state;
      break;
  }
}
