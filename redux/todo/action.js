// @flow
import {
  CREATE_TODO,
  DELETE_TODO,
  UPDATE_TODO,
  RESET_FORM
} from "./actionType";

export function deleteTodo(id: number) {
  return {
    type: DELETE_TODO,
    payload: id
  };
}
export function createTodo(data: Object) {
  return {
    type: CREATE_TODO,
    payload: data
  };
}
export function updateTodo(id: number, data: Object) {
  return {
    type: UPDATE_TODO,
    payload: { id, data }
  };
}
export function resetForm() {
  return {
    type: RESET_FORM
  };
}
