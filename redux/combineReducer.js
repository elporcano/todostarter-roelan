import { combineReducers } from "redux";
import todoReducer from "./todo/reducer";

const reducer = combineReducers({
  todo: todoReducer
});

export default reducer;
