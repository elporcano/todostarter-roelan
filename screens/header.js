import React, { Component } from "react";
import { View, Text, StyleSheet, Keyboard, Dimensions } from "react-native";

export default class Header extends Component {
  state = {
    headerTitle: {}
  };
  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = e => {
    // let newSize = Dimensions.get("window").height - e.endCoordinates.height;
    this.setState({
      headerTitle: { fontSize: 17, padding: 20 }
    });
  };

  _keyboardDidHide = e => {
    this.setState({
      headerTitle: { fontSize: 25, padding: 40 }
    });
  };
  render() {
    return (
      <View style={styles.header}>
        <Text style={[styles.headerTitle, this.state.headerTitle]}>
          {this.props.header}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flex: 1,
    padding: 5,
    backgroundColor: "#ffcc00"
  },
  headerTitle: {
    padding: 40,
    fontSize: 25,
    textAlign: "center",
    color: "#000",
    letterSpacing: 3
    // height: 80,
    // textShadowColor: '#111',
    // textShadowOffset: { width: 1, height: 2 },
    // textShadowRadius: 8
  }
});
