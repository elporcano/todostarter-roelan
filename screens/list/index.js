import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Alert
} from "react-native";
import { Ionicons, Feather } from "react-native-vector-icons";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { deleteTodo } from "./../../redux/todo/action";

import styles from "./style";
import Header from "./../header";
import Button from "../../components/Button/index";

class ListScreen extends Component {
  constructor(props) {
    super(props);

    const current_date = new Date().toDateString();
    this.state = {
      pressEdit: false,
      pressDel: false,
      pressEditId: 0,
      pressDelId: 0
    };
  }

  fn_rows = () => {
    const state = this.props.dataList;
    return Object.keys(state).map(elem => {
      return {
        id: elem,
        ...state[elem]
      };
    });
  };

  gotoDetails = item => {
    this.props.navigation.navigate("Details", { ...item });
  };

  gotoUpdate = item => {
    // If action was clicked update the state to change the Icon into active color.
    this.setState({ pressEdit: true, pressId: item.id });

    this.props.navigation.navigate("Update", { ...item });
  };

  deleteRow = item => {
    this.setState(
      () => ({
        pressDel: true,
        pressDelId: item.id
      }),
      () => {
        Alert.alert(
          "Confirmation",
          "Are you sure you want to Delete this todo?",
          [
            {
              text: "Cancel",
              onPress: () => {
                return;
              }
            },
            {
              text: "Ok",
              onPress: () => {
                this.props.deleteTodo(item.id);
              }
            }
          ]
        );
      }
    );
  };

  extractKey = ({ id }) => id;

  renderItem = ({ item }) => {
    return (
      <View style={styles.listContainer}>
        {/* View details */}
        <TouchableOpacity
          style={styles.title}
          onPress={() => this.gotoDetails(item)}
        >
          <View>
            <Text numberOfLines={5} ellipsizeMode={"tail"}>
              {item.title}
            </Text>
          </View>
        </TouchableOpacity>
        {/* Action update */}
        <Button onPress={() => this.gotoUpdate(item)}>
          <Feather name="edit-3" size={22} color={"gray"} />
        </Button>
        {/* Action delete */}
        <Button onPress={() => this.deleteRow(item)}>
          <Ionicons name="ios-remove-circle" size={22} color={"gray"} />
        </Button>
      </View>
    );
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header header="List" />
        <View style={styles.content}>
          <FlatList
            style={styles.container}
            data={this.fn_rows()}
            renderItem={this.renderItem}
            keyExtractor={this.extractKey}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  dataList: state.todo.data
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      deleteTodo: id => deleteTodo(id)
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ListScreen);
