import { StyleSheet } from "react-native";

export default StyleSheet.create({
  content: {
    flex: 5,
    margin: 15
  },
  listContainer: {
    flexDirection: "row",
    paddingTop: 15,
    paddingBottom: 15,
    borderBottomColor: "black",
    borderBottomWidth: 1,
    marginBottom: 15
  },
  title: {
    width: "70%"
  }
});
