import React, { Component } from "react";
import { KeyboardAvoidingView, Platform } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Header from "./../header";
import Form from "./../form";
import { updateTodo } from "./../../redux/todo/action";

class UpdateScreen extends Component {
  render() {
    const item = this.props.route.params;
    return (
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === "ios" ? "padding" : null}
        // keyboardVerticalOffset={Platform.OS === "ios" ? 50 : 0}
      >
        <Header header="Update your Todo" />
        <Form
          buttonActionTitle="Update todo"
          actionTitle="update"
          actionButton={(id, data) => this.props.dispatch(updateTodo(id, data))}
          fillData={item}
        />
      </KeyboardAvoidingView>
    );
  }
}

export default connect()(UpdateScreen);
