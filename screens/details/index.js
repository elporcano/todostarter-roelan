import React, { Component } from "react";
import { View, Text } from "react-native";
import styles from "./style";

export default class DetailsScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { title, description, todo_at } = this.props.route.params;
    const _todo_at = new Date(todo_at).toDateString();
    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.date}>{_todo_at}</Text>
        </View>
        <View style={styles.content}>
          <Text style={styles.contentText}>{description}</Text>
        </View>
      </View>
    );
  }
}
