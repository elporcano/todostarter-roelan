import { StyleSheet } from "react-native";
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffcc00"
  },
  titleContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#ffcc00"
  },
  title: {
    textAlign: "center",
    width: "70%",
    fontSize: 30
  },
  date: {
    width: "30%",
    textAlign: "center",
    fontSize: 18,
    borderLeftColor: "gray",
    borderLeftWidth: 1
  },
  content: {
    flex: 5,
    backgroundColor: "#f2f2f2",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    padding: 20
  },
  contentText: {
    color: "#111"
  }
});
