//import all screens here.

import CreateScreen from './create/index';
import UpdateScreen from './update/index';
import ListScreen from './list/index';
import DetailsScreen from './details/index';

export {
    CreateScreen,
    UpdateScreen,
    ListScreen,
    DetailsScreen
}
