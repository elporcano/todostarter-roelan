import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Button as RNButton,
  Platform
} from "react-native";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { resetForm } from "../redux/todo/action";
import Button from "../components/Button/index";
import Input from "../components/Input/index";
import Datepicker from "../components/Datepicker/index";
import DateTimePicker from "@react-native-community/datetimepicker";

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.fillData.id || "",
      title: this.props.fillData.title || "",
      description: this.props.fillData.description || "",
      todo_at: this.props.fillData.todo_at || null
    };
  }

  formValidation = () => {
    const { title, description, todo_at } = this.state;
    if (title.trim() === "") {
      this.setState(prevState => {
        return {
          errors: {
            ...prevState.errors,
            title: "this field is required"
          }
        };
      });
    }
    if (description.trim() === "") {
      this.setState(prevState => {
        return {
          errors: {
            ...prevState.errors,
            description: "this field is required"
          }
        };
      });
    }
    if (todo_at.trim() === "") {
      this.setState(prevState => {
        return {
          errors: {
            ...prevState.errors,
            todo_at: "this field is required"
          }
        };
      });
    }
  };

  onSubmit = () => {
    const { id, title, description, todo_at } = this.state;
    console.log("State:", this.state);
    if (this.props.actionTitle === "add") {
      this.props.actionTodo({ title, description, todo_at });
    } else {
      this.props.actionButton(id, { title, description, todo_at });
    }
  };

  onChangeText = (fieldName, val) => {
    this.setState({
      [fieldName]: val
    });
  };

  onChangeDate = selectedDate => {
    this.setState({
      todo_at: selectedDate
    });
  };

  componentDidUpdate(prevProps) {
    if (this.props.isComplete !== prevProps.isComplete) {
      this.clearError();
      if (this.props.actionTitle === "add") {
        this.clearInputs();
      }
    }
  }

  clearInputs() {
    this.setState({ title: "", description: "", todo_at: "" });
  }

  clearError() {
    this.props.resetForm(); //It will clear also error message
  }

  render() {
    const { title, description, todo_at, showDatePicker } = this.state;

    return (
      <View style={styles.content}>
        <View>
          {this.props.errors.map((msg, indx) => (
            <Text key={indx}>{msg}</Text>
          ))}
        </View>
        <Input
          placeholder="Title"
          onChangeText={val => this.onChangeText("title", val)}
          value={title}
        />
        <Input
          placeholder="Description"
          onChangeText={val => this.onChangeText("description", val)}
          value={description}
        />
        <Datepicker setDate={todo_at} onChangeDate={this.onChangeDate} />
        <Button
          styles={styles.button}
          onPress={this.onSubmit}
          label={this.props.buttonActionTitle}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 5,
    margin: 15
  },
  button: {
    backgroundColor: "blue",
    marginTop: 20,
    padding: 15,
    width: "100%"
  }
});

const mapStateToProps = state => {
  return {
    errors: state.todo.errors,
    data_: state.todo.getOne,
    isComplete: state.todo.isComplete
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return bindActionCreators(
    {
      actionTodo: val => ownProps.action(val),
      resetForm: () => resetForm()
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);
