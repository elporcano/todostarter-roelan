import React, { Component } from "react";
import { KeyboardAvoidingView, Platform } from "react-native";
import Header from "./../header";
import Form from "./../form";
import { createTodo } from "./../../redux/todo/action";

export default class CreateScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === "ios" ? "padding" : null}
        // keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}
      >
        <Header header="Create your Todo" />
        <Form
          buttonActionTitle="Add todo"
          actionTitle="add"
          action={createTodo}
          fillData={{}}
        />
      </KeyboardAvoidingView>
    );
  }
}
